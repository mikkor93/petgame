﻿using UnityEngine;
using System.Collections;

public class UserEventHandler : MonoBehaviour {
    public enum ExerciseType {
        None,
        Walk,
        Run
    }

    public float refreshRate = 1f;

    public ExerciseType exerciseType;


    #region Exercise coroutines
    IEnumerator Exercise() {
        while (true) {
            if(exerciseType == ExerciseType.Walk) {
                Debug.Log("Walking");
                //Get pedometer data
                //Get GPS data
            }

            if(exerciseType == ExerciseType.Run) {
                Debug.Log("Run");
                //Get pedometer data
                //Get GPS data
            }

            yield return new WaitForSeconds(refreshRate);
        }
    }
    /*IEnumerator Walking() {
        while (true) {
            Debug.Log("Walking");
            //Get pedometer data
            //Get GPS data

            yield return new WaitForSeconds(refreshRate);
        }
    }*/
    #endregion

    #region Exercise
    public void AddEvent_Exercise_Walk() {
        exerciseType = ExerciseType.Walk;
        StartCoroutine(Exercise());
    }

    public void AddEvent_Exercise_Run() {
        exerciseType = ExerciseType.Run;
        StartCoroutine(Exercise());
    }

    public void AddEvent_Exercise_EndExercise() {
        exerciseType = ExerciseType.None;
        StopCoroutine(Exercise());
    }
    #endregion
}

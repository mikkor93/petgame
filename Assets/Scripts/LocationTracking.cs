﻿using UnityEngine;
using System.Collections;

public class LocationTracking : MonoBehaviour {

    public bool isMale;
    public float playerHeightCm;
    public float numberOfSteps;
    public int trackingInterval = 5;

    public float overallDistance;
    public int maxWaitTime = 20;
    private int timesTracked;
    private float stepLenght = 0.74f;
    private string string1;
    private string string2;
    private string string3;
    private string string4;

    private float lonA;
    private float latA;

    public string mapLink;
    private string midPointsString;
    private float mapStartLon;
    private float mapStartLat;

    void Start ()
    {
        // Check if male or female, calculate step lenght accordingly
        if (isMale)
            stepLenght = playerHeightCm * 0.415f * 0.01f;
        if (!isMale)
            stepLenght = playerHeightCm * 0.413f * 0.01f;

        //Invoke("Track", 5);
        StartCoroutine("StartTracking");
	}

    IEnumerator StartTracking ()
    {
        string1 = "Starting";
        // Check is location service enabled
        if (!Input.location.isEnabledByUser)
            yield break;

        // Start service
        Input.location.Start(5); // With 5m accuracy

        // Wait service to start up
        while (Input.location.status == LocationServiceStatus.Initializing && maxWaitTime > 0)
        {
            yield return new WaitForSeconds(1);
            maxWaitTime--;
        }

        // Service failed to start within the maxWaitTime
        if(maxWaitTime < 1)
        {
            string2 = "Timed out";
            yield break;
        }

        // Connection failed
        if(Input.location.status == LocationServiceStatus.Failed)
        {
            string3 = "Unable to determine device location";
            yield break;
        }

        // Access granted and could get the location value
        else
        {
            string2 = "Succesfully initiated, starting tracking..";
            InvokeRepeating("Track", 1, trackingInterval);

            lonA = Input.location.lastData.longitude;
            latA = Input.location.lastData.latitude;

            mapStartLon = lonA;
            mapStartLat = latA;
        }

        // Stop service if there is no need to query location updates continously
        // Input.location.Stop();
    }

    void Track()
    {
        string3 = "Times tracked " + timesTracked;
        /*string4 = ("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude
        + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy +
        " " + Input.location.lastData.timestamp);*/

        // Calculate distance between present location and last location and add that to overall distance travelled
        CalculateDistance(lonA, latA, Input.location.lastData.longitude, Input.location.lastData.latitude);

        // Update the "last data" of location
        lonA = Input.location.lastData.longitude;
        latA = Input.location.lastData.latitude;

        timesTracked++;
    }

    public void CalculateDistance (float oldLon, float oldLat, float newLon, float newLat)
    {
        // Calculate the distance from coordinates to meters of the new and old values
        float distLon = Radians(oldLon - newLon);
        float distLat = Radians(oldLat - newLat);

        float distance = Mathf.Pow(Mathf.Sin(distLat / 2), 2) + Mathf.Cos(Radians(oldLat)) * 
            Mathf.Cos(Radians(newLat)) * Mathf.Pow(Mathf.Sin(distLon / 2), 2);

        float c = 2 * Mathf.Atan2(Mathf.Sqrt(distance), Mathf.Sqrt(1 - distance));

        float distInMeters = 6371 * c * 1000;

        // Calculate rough amount of steps


        string1 = stepLenght.ToString();

        numberOfSteps = overallDistance / stepLenght;

        // Add the distance travelled after the last sync to the total distance travelled
        overallDistance += distInMeters;

        string4 = overallDistance.ToString("F1") + " meters. " + numberOfSteps.ToString("F1") + " steps.";

        // Update map link string
        UpdateMapLink(newLon, newLat);
    }

    public static float Radians(float x)
    {
        return x * Mathf.PI / 180;
    }

    void UpdateMapLink (float newLon, float newLat)
    {
        Debug.Log("Creating map link");
        midPointsString = midPointsString + "/@" + newLat + "," + newLon;
        mapLink = "https://www.google.fi/maps/dir/" + mapStartLat + "," + mapStartLon + "/" + newLat + "," + newLon + "/@" + midPointsString;
        string1 = mapLink;
    }

    void OnGUI ()
    {
        GUILayout.Label(string1);
        GUILayout.Label(string2);
        GUILayout.Label(string3);
        GUILayout.Label(string4);
    }
}

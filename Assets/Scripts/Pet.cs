﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Pet : MonoBehaviour {

    [Header("Player Attributes")]
    public string playerName = "Davind the test block";
    public int playerLevel;

    public string currentTime;

    //These are in seconds
    public int timeSlept = 0;
    public int timeAwake = 0;
    //Times in seconds to stay awake
    public int timeToStayAwake = 57600;
    //and sleep
    public int timeToStayAsleep = 28800;
    //and when to eat
    public int timeBetweenMeals = 10800;
    int mealTimer = 0;

    public bool isSleeping;
    public bool isAwake;

    [Header("UI")]
    public Slider healthSlider;
    public Slider hungerSlider;
    public Slider sleepSlider;
    public Text playerNameText;
    public Text timeText;
    public Text playerLevelText;
    public GameObject popup;


    void Start () {
        //Set player name
        playerNameText.text = playerName;

        //Coroutines
        //Update the clock
        StartCoroutine(UpdateClock());
        //Update player attributes
        StartCoroutine(UpdateAttributes());
        StartCoroutine(BeAwake());
        //Get player information

        //Set sleep and awake times
        sleepSlider.maxValue = timeToStayAsleep;
        sleepSlider.value = timeToStayAsleep;
        hungerSlider.maxValue = timeBetweenMeals;
        hungerSlider.value = timeBetweenMeals;

    }

    #region 
    IEnumerator UpdateClock() {
        timeText.text = System.DateTime.Now.TimeOfDay.Hours + ":" + System.DateTime.Now.TimeOfDay.Minutes;
        yield return new WaitForSeconds(1);
    }

    IEnumerator UpdateAttributes() {
 
        yield return new WaitForSeconds(1);
    }

    IEnumerator StartSleeping() {
        while(isSleeping) {
            Debug.Log("Sleeping");
            timeSlept++;
            sleepSlider.value++;

            yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator BeAwake() {
        while(isAwake) {
            Debug.Log("Is awake");
            timeAwake++;
            mealTimer++;
            sleepSlider.value--;
            hungerSlider.value--;

            //Alert when it's time to have a meal
            if(mealTimer >= timeBetweenMeals) {
                TimeToEat();
            }

            yield return new WaitForSeconds(1f);
        }
    }
    #endregion

    public void WakeUp() {
        isSleeping = false;
        isAwake = true;

        StartCoroutine(BeAwake());
        StopCoroutine(StartSleeping());

        //Reset values
        timeSlept = 0;
    }

    public void Sleep() {
        isSleeping = true;
        isAwake = false;

        StartCoroutine(StartSleeping());
        StopCoroutine(BeAwake());


        //reset some values
        timeAwake = 0;
    }

    public void AddExercise(int exerciseStress) {
        hungerSlider.value -= exerciseStress;
    }

    public void MealOK() {
        //Reset values
        hungerSlider.value = hungerSlider.maxValue;
        mealTimer = 0;
    }

    private void TimeToEat() {
        popup.SetActive(true);

        
    }
}

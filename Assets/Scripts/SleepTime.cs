﻿using UnityEngine;
using System.Collections;
using System;

public class SleepTime : MonoBehaviour {

    public bool isSleeping;
    public DateTime startedSleeping;
    public DateTime wokeUp;
    public TimeSpan sleepTime;

	void Start ()
    {
    //    SetSleeping ();
    //    Invoke("SetSleeping", 5f);
    }

    void SetSleeping ()
    {
        Debug.Log("Running");
        // Toggle between sleeping / not sleeping
        if(!isSleeping)
        {
            isSleeping = true;
            //Debug.Log(DateTime.Now + isSleeping.ToString());
            startedSleeping = DateTime.Now;
        }
        else
        {
            isSleeping = false;
            //Debug.Log(DateTime.Now + isSleeping.ToString());
            wokeUp = DateTime.Now;
            sleepTime = wokeUp - startedSleeping;
            //Debug.Log("Slept for " + sleepTime);
            Debug.Log("Slept for " + sleepTime.Hours + " hours, " + sleepTime.Minutes + " minutes and " + sleepTime.Seconds + " seconds.");
        }
    }
}

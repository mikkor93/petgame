﻿using UnityEngine;
using System.Collections;

public enum activityStage { Light, Medium, High };

public class CaloryConsumption : MonoBehaviour {

    public bool isMale;
    public int weightKg = 80;
    public int heightCm = 180;
    public int ageY = 25;
    public activityStage activity;
    public float caloriesRequired;
    private float bmr;

	void Start ()
    {
        CalculateCalories();
    }

	void CalculateCalories ()
    {
        float multiplier = 0;

        string activityString = activity.ToString();
        switch (activity)
        {
            case activityStage.Light:
                multiplier = 1.2f;
                break;

            case activityStage.Medium:
                multiplier = 1.375f;
                break;

            case activityStage.High:
                multiplier = 1.55f;
                break;
        }

        if (isMale)
        {
            // Male BMR value maintaining current weight
            bmr = (10 * weightKg) + (6.25f * heightCm) - (5 * ageY) + 5;
            caloriesRequired = bmr * multiplier;
        }

        else
        {
            // Female BMR value maintaining current weight
            bmr = (10 * weightKg) + (6.25f * heightCm) - (5 * ageY) -161;
            caloriesRequired = bmr * multiplier;
        }
    }
}
